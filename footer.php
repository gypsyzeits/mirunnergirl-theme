<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>
<div class="scroll-top-wrapper"><a class="scroll-top" href="#scroll-to-top-target"><h3 class="font-color-primary"><i class="fa fa-chevron-up"></i> Back to Top</h3></a></div>
<footer class="footer">
    
    <div class="footer-container">
        <div class="footer-grid" dis>
            <?php dynamic_sidebar( 'footer-widgets' ); ?>
        </div>

        <div class="grid-container">
            <div class="grid-x grid-margin-x grid-margin-y">
                <div class="cell medium-4">
                    <div class="grid-x">
                        <div class="cell margin-top margin-bottom">
                            <h5 class="text-center subheading">Subscribe</h5>
                            <p class="text-center">Subscribe to my newsletter. I'll keep you updated with fitness tips, recipes, and more!</p></div>
                    </div>
                <div class="relative" style="z-index: 3;"><?php echo do_shortcode( get_field('mailchimp_shortcode',get_option( 'page_on_front' )), $ignore_html = false ); ?></div>
                </div>
                <div class="cell medium-6">
                    <div class="grid-x">
                    <div class="cell medium-6"><?php wp_nav_menu( array('theme_location'=>'footer_about'));  ?></div>
                    <div class="cell medium-6"><?php wp_nav_menu( array('theme_location'=>'footer_podcast'));  ?></div>
                    <div class="cell medium-6"><?php wp_nav_menu( array('theme_location'=>'footer_blog'));  ?></div>
                    <div class="cell medium-6 special-menu"><?php wp_nav_menu( array('theme_location'=>'footer_last'));  ?></div>
                    </div>
                </div> 
                <div class="cell medium-2" style="text-align: center;">
                    <p class="text-center social-icon"><a href="<?php the_field('facebook_url',get_option( 'page_on_front' )); ?>"><i class="social-icon-round fa fa-facebook-f"></i></a></p>
                    <p class="text-center social-icon"><a href="<?php the_field('twitter_url',get_option( 'page_on_front' )); ?>"><i class="social-icon-round fa fa-twitter"></i></a></p>
                    <p class="text-center social-icon"><a href="<?php the_field('instagram_url',get_option( 'page_on_front' )); ?>"><i class="social-icon-round fa fa-instagram"></i></a></p>
                    <p class="text-center social-icon"><a href="<?php the_field('pinterest_url',get_option( 'page_on_front' )); ?>"><i class="social-icon-round fa fa-pinterest"></i></a></p>
                    <p class="text-center social-icon"><a href="<?php the_field('youtube_url',get_option( 'page_on_front' )); ?>"><i class="social-icon-round fa fa-youtube"></i></a></p>
                </div>
                <div class="cell"><p class="text-center"><a href="http://novumproductions.com/">Design & Development by <img width="72px" src="<?php echo get_template_directory_uri(); ?>/dist/assets/images/NovumProductionsLogo.png"></a></p></div>
            </div>
        </div>
    </div>
</footer>
<script>
jQuery(document).ready(function($){
$(".mc4wp-checkbox-woocommerce label span").html("Get exclusive deals, hear about special Mitten State events, be inspired by stories of everyday people doing interesting things outdoors, and learn about the best places to explore in the Great Lakes State.")
$(".mc4wp-checkbox-woocommerce label input").prop('checked', true);
}); 
</script>
<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
    </div><!-- Close off-canvas content -->
<?php endif; ?>

<?php wp_footer(); ?>

</body>
</html>