<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' );
get_template_part( 'template-parts/woocommerce-header-helper' );
?>

<?php

		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		remove_action('woocommerce_before_main_content','woocommerce_breadcrumb', 20);
		do_action( 'woocommerce_before_main_content' );
		?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

		<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
		?>

		<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
		?>
		<?php 
		global $post;
		if ($post->post_name === 'event-submission'):
			?>
			<script type="text/javascript">
				$('document').ready( function(){
					$('.quantity').hide();
					$('.woocommerce-Price-amount').hide();
					$('#gform_totals_1 li:first-child').hide();
					$('#gform_totals_1 li:nth-child(2)').hide();
					$('.event-location-lng input').attr('data-geo','lat');
					$('.event-location-lat input').attr('data-geo','lng');

					$('.event-location-input input').after('<li><div id="map-picker"></div></li>');
					$('#map-picker').hide();

					$('.event-location-input input').geocomplete(
					{
						details:'.gform_body', 
						detailsAttribute: 'data-geo',
						map:'#map-picker', 
						mapOptions: {
							zoom: 10
						},
					} );
					$('.event-location-input input').geocomplete().bind('geocode:result', function(event, result){
						$('#map-picker').show();
					})
				});

			</script>



		<?php endif; ?>
		<?php get_footer( 'shop' );

		/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
