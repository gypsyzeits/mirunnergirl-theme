<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header( 'shop' );



?>
<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.

$seasons = array( 	
	'winter'	=> get_id_by_slug('home-winter'),
	'spring'	=> get_id_by_slug('home-winter/home-spring'),
	'summer'	=> get_id_by_slug('home-winter/home-summer'), 
	'fall'		=> get_id_by_slug('home-winter/home-fall')); 

	?>

	<?php foreach ($seasons as $key => $value): ?>
		<header class="featured-hero <?php echo $key.'-content' ?>" role="banner" data-interchange="[<?php echo get_the_post_thumbnail_url( $value, 'featured-small' ); ?>, small], [<?php echo get_the_post_thumbnail_url( $value, 'featured-medium' ); ?>, medium], [<?php echo get_the_post_thumbnail_url( $value, 'featured-large' ); ?>, large], [<?php echo get_the_post_thumbnail_url( $value, 'featured-xlarge' ); ?>, xlarge]">
			<h1 class="page-hero-name text-center font-color-white text-shadow">SHOP</h1>
		</header>
	<?php endforeach ?>

	<?php $seasons = array( 	
		'winter'	=> get_id_by_slug('home-winter'),
		'spring'	=> get_id_by_slug('home-winter/home-spring'),
		'summer'	=> get_id_by_slug('home-winter/home-summer'), 
		'fall'		=> get_id_by_slug('home-winter/home-fall'));  ?>
		<div class="grid-container-full-width">
			<div class="grid-x">
				<div class="cell small-12  bg-primary-color">
					<p class="text-center store-announcement-wrapper subheading font-color-white margin-top">
						<span class="season-icon-wrapper">
							<?php foreach ($seasons as $key => $value):?>
								<img class="season-icon <?php echo $key.'-content' ?>" src="<?php echo get_template_directory_uri().'/dist/assets/images/'.$key.'-icon.png' ?>">
							<?php endforeach; ?>
						</span>
						<span class=store-announcement><?php the_field('shop_announcement', wc_get_page_id('shop')); ?></span>
						<span class="season-icon-wrapper">
							<?php foreach ($seasons as $key => $value):?>
								<img class="season-icon <?php echo $key.'-content' ?>" src="<?php echo get_template_directory_uri().'/dist/assets/images/'.$key.'-icon.png' ?>">
							<?php endforeach; ?>
						</span>
					</p>
				</div>
				<div class="cell small-12 margin-top show-for-medium shop-heading-menu">
					<?php wp_nav_menu( array('theme_location'=>'shop_heading_menu'));  ?>
				</div>
				<?php if (is_shop()): ?>
					<div class="cell small-12 gradient-primary-md-gray">
						<?php get_template_part( 'template-parts/testimonials-slider' ); ?>
					</div>
				<?php endif; ?>

			</div>
		</div>
		<div class="main-container-full-width">
			<div class="main-grid">
				<main class="main-content-full-width">
					<div class="grid-container">
						<div class="grid-x">
							<?php

							/**
					 * Hook: woocommerce_before_main_content.
					 *
					 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
					 * @hooked woocommerce_breadcrumb - 20
					 * @hooked WC_Structured_Data::generate_website_data() - 30
					 */

							remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
							do_action( 'woocommerce_before_main_content' );
						/**
						 * Hook: woocommerce_archive_description.
						 *
						 * @hooked woocommerce_taxonomy_archive_description - 10
						 * @hooked woocommerce_product_archive_description - 10
						 */
						?>
						<div class="small-12 text-center">
						<?php do_action( 'woocommerce_archive_description' );
						?>
						</div>
					</header>
					<?php

					if ( have_posts() ) {

						/**
						 * Hook: woocommerce_before_shop_loop.
						 *
						 * @hooked wc_print_notices - 10
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
						do_action( 'woocommerce_before_shop_loop' );

						woocommerce_product_loop_start();

						if ( wc_get_loop_prop( 'total' ) ) {
							while ( have_posts() ) {
								the_post();

								/**
								 * Hook: woocommerce_shop_loop.
								 *
								 * @hooked WC_Structured_Data::generate_product_data() - 10
								 */
								do_action( 'woocommerce_shop_loop' );

								wc_get_template_part( 'content', 'product' );
							}
						}

						woocommerce_product_loop_end();

						/**
						 * Hook: woocommerce_after_shop_loop.
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', $priority = 10 );
						add_action('woocommerce_after_shop_loop', 'foundationpress_pagination', 10);
						do_action( 'woocommerce_after_shop_loop' );


					} else {
						/**
						 * Hook: woocommerce_no_products_found.
						 *
						 * @hooked wc_no_products_found - 10
						 */
						do_action( 'woocommerce_no_products_found' );
					}

					/**
					 * Hook: woocommerce_after_main_content.
					 *
					 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
					 */
					do_action( 'woocommerce_after_main_content' );

					/**
					 * Hook: woocommerce_sidebar.
					 *
					 * @hooked woocommerce_get_sidebar - 10
					 */
					//do_action( 'woocommerce_sidebar' );

					?>
				</div>
			</div>
		</main>
	</div>
</div>
<?php
get_footer( 'shop' );
