<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>
<?php if ( has_post_thumbnail( get_option('page_for_posts') ) ) : ?>
	<header class="featured-hero" role="banner" data-interchange="[<?php echo get_the_post_thumbnail_url( get_option('page_for_posts'), 'featured-small' ); ?>, small], [<?php echo get_the_post_thumbnail_url( get_option('page_for_posts'), 'featured-medium' ); ?>, medium], [<?php echo get_the_post_thumbnail_url( get_option('page_for_posts'), 'featured-large' ); ?>, large], [<?php echo get_the_post_thumbnail_url( get_option('page_for_posts'), 'featured-xlarge' ); ?>, xlarge]">
		
		<h1 class="page-hero-name text-center font-color-white text-shadow"><?php the_field('hero_name', get_option('page_for_posts'));?></h1>
		<h3 class="page-hero-subtitle text-center font-color-white subheading text-shadow"><?php the_field('hero_subheading', get_option('page_for_posts'));?></h3>
	</header>
<?php else: ?>
	<?php $seasons = array( 	
	'winter'	=> get_id_by_slug('home-winter'),
	'spring'	=> get_id_by_slug('home-winter/home-spring'),
	'summer'	=> get_id_by_slug('home-winter/home-summer'), 
	'fall'		=> get_id_by_slug('home-winter/home-fall')); 
	?>
<?php foreach ($seasons as $key => $value): ?>
		<header class="featured-hero <?php echo $key.'-content' ?>" role="banner" data-interchange="[<?php echo get_the_post_thumbnail_url( $value, 'featured-small' ); ?>, small], [<?php echo get_the_post_thumbnail_url( $value, 'featured-medium' ); ?>, medium], [<?php echo get_the_post_thumbnail_url( $value, 'featured-large' ); ?>, large], [<?php echo get_the_post_thumbnail_url( $value, 'featured-xlarge' ); ?>, xlarge]">
			<h1 class="page-hero-name text-center font-color-white text-shadow"><?php the_field('hero_name', get_option('page_for_posts'));?></h1>
		<h3 class="page-hero-subtitle text-center font-color-white subheading text-shadow"><?php the_field('hero_subheading', get_option('page_for_posts'));?></h3>
		</header>
	<?php endforeach ?>
<?php endif; ?>


<div class="main-container-full-width">
	<div class="main-grid">
		<main class="main-content-full-width">
			<div class='grid-container blog-card-loop'>
				<div class="grid-x grid-margin-y grid-margin-x">
					<div class="cell">
						<h1 class="text-center font-color-primary"><?php the_field('header', get_option('page_for_posts'));?></h1>
						<p class="text-center"><?php the_field('header_subheading', get_option('page_for_posts'));?></p>
					</div>
	<?php if ( have_posts() ) : ?>

		<?php /* Start the Loop */ ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'template-parts/content-card-3', get_post_type() ); ?>
		<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; // End have_posts() check. ?>

		<?php /* Display navigation to next/previous pages when applicable */ ?>
		<?php
		if ( function_exists( 'foundationpress_pagination' ) ) :
			?>
			<div class="cell small-12 text-center">
			<?php foundationpress_pagination(); ?>
			</div>
			<?php
		elseif ( is_paged() ) :
		?>


			<nav id="post-nav">
				<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
				<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
			</nav>
		
		<?php endif; ?>

	</main>

	</div>
</div>

<?php get_footer();
