<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.get_permalink( woocommerce_get_page_id( 'shop' ) );
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header();

$seasons = array( 	'winter'	=> get_id_by_slug('home-winter'),
	'spring'	=> get_id_by_slug('home-winter/home-spring'),
	'summer'	=> get_id_by_slug('home-winter/home-summer'), 
	'fall'		=> get_id_by_slug('home-winter/home-fall'));
//var_dump($seasons);
	?>

	<div class="main-container-full-width">
		<div class="main-grid">
			<div class="row expanded hero-wrapper" style="position: relative;">

				<div class="hero-content">
					<h2 class="font-color-primary text-shadow"><?php the_field('hero_title');?></h2>
					<h3 class="subheading"><?php the_field('hero_subtitle');?></h3>
					<div class="grid-x grid-margin-y subhero-content">
						<div class="cell small-10 small-offset-1 medium-4 medium-offset-1"><a href="<?php the_field('hero_button_1_link'); ?>"><button class="sites-button subheading"><?php the_field('hero_button_1_text'); ?></button></a></div>
						<div class="cell small-10 small-offset-1 medium-4 medium-offset-2"><a href="<?php the_field('hero_button_2_link'); ?>"><button class="sites-button subheading"><?php the_field('hero_button_2_text'); ?></button></a></div>
					</div>
				</div>

				<?php foreach ($seasons as $season => $season_id): ?>
					<div class="<?php echo $season;?>-content">
						<?php if (get_field("hero_video", $season_id)): ?>
							<div class="video-background" style="display: none;">
								<div class="video-foreground">
									<iframe id="<?php echo $season."-video" ?>" class="season-video" src="" data-yturl="https://www.youtube.com/embed/<?php echo the_field("hero_video", $season_id); ?>?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&mute=1&playlist=<?php echo the_field("hero_video", $season_id); ?>&enablejsapi=1" frameborder="0" allowfullscreen></iframe>
								</div>
							</div>
						<?php endif ?>

						<div class="orbit fp-hero" role="region" aria-label="Hero-<?php echo $season ?>" data-orbit>
							<div class="orbit-wrapper">

								<ul class="orbit-container">
									<?php for ($i = 1; $i <= 3; $i++) {
										?>
										<li class="orbit-slide">
											<figure class="orbit-figure">


												<span style="background-image: url('<?php echo get_field('hero_slide_' . $i, $season_id) ?>');"></span>
											</figure>
										</li>

										<?php
									}?>
								</ul>
							</div>
							<nav class="orbit-bullets">
								<button class="is-active" data-slide="0"><img src="<?php echo get_template_directory_uri() . '/dist/assets/images/logo-bullet.png' ?>"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
								<button data-slide="1"><img src="<?php echo get_template_directory_uri() . '/dist/assets/images/logo-bullet.png' ?>"><span class="show-for-sr">Second slide details.</span></button>
								<button data-slide="2"><img src="<?php echo get_template_directory_uri() . '/dist/assets/images/logo-bullet.png' ?>"><span class="show-for-sr">Third slide details.</span></button>
							</nav>
						</div>
					</div>
				<?php endforeach; ?>

			</div>
			<!-- Main Content Section, hooked up to editor for front page announcements -->
			<main class="main-content-full-width">
				<?php while (have_posts()): the_post();?>
					<?php //get_template_part( 'template-parts/content', 'page' ); ?>
					<?php //comments_template(); ?>
				<?php endwhile;?>
			</main>

			<!-- Welcome to Mi Runner Girl, 4 columns block -->
			<div class="grid-container margin-top margin-bottom" >
				<div class="grid-x centered grid-margin-x" data-equalizer data-equalize-on="medium" id="test-eq">
					<div class="cell small-12"><h1 class="text-center margin-top">Welcome To Michigan Runner Girl</h1></div>
					<div class="cell small-12"><h2 class="text-center subheading font-color-primary margin-top margin-bottom">Healthy Body, Healthy Mind</h2></div>
					<?php for ($i=1; $i < 5 ; $i++):?>
						<div class="nav-card cell large-3 medium-6 small-12" data-equalizer-watch>
							<?php foreach ($seasons as $season => $season_id): ?>
								<span class="image expanded <?php echo "$season-content" ?>" style="background-image: url(<?php the_field('card_'.$i.'_image', $season_id)?>);"></span>
								<?php endforeach; ?>
								<?php the_field('card_'.$i.'_text'); ?>
								<a href="<?php the_field('card_'.$i.'_button_link'); ?>"><button class="sites-button subheading"><?php the_field('card_'.$i.'_button_text'); ?></button></a>
							</div>
						<?php endfor; ?>

					</div>
				</div>
			</div></div>
			<?php echo do_shortcode('[sponsors_row]'); ?>


			<!-- MRG Youtube Channel Block -->
			<div class="row expanded margin-top">
				<div class="grid-x">
					<div class="cell small-12"><h1 class="text-center font-color-secondary">MRG Youtube Channel</h1></div>
					<div class="cell small-12"><p class="text-center margin-top margin-bottom"><a href="https://www.youtube.com/channel/UCTdVGFXBtFr8xo000Zj12uw"><button class="sites-button button-contract subheading">View Channel</button></a></p></div>
					<div class="cell small-12"><div class="responsive-embed" style="padding-bottom: 60%;"><iframe width="100%"" height="auto" src="https://www.youtube.com/embed/?listType=playlist&list=UUTdVGFXBtFr8xo000Zj12uw" frameborder="0" allowfullscreen /></iframe></div></div>

				</div>
			</div>


			<!-- Gear For All seasons block -->
			<div class="grid-container gear-section margin-top margin-bottom">
				<div class="grid-x grid-margin-x grid-margin-y">
					<div class="cell small-12 margin-top margin-bottom"><p><?php the_field('store_blurb'); ?></p></div>
					<div class="cell medium-6 shop-card pad">
						<a href="<?php the_field('block_1_link'); ?>">
							<?php foreach ($seasons as $season => $season_id): ?>
								<span class="image box-shadow <?php echo "$season-content" ?>" style="background-image: url(<?php the_field('block_1_image', $season_id)?>)"></span>
							<?php endforeach; ?>
							<h1 class="text-center font-color-white" style="padding-left: 1rem"><?php the_field('block_1_text'); ?></h1>
						</a>
					</div>
					<div class="cell medium-6 ">
						<div class="grid-x grid-margin-x grid-margin-y">
							<div class="cell small-12 shop-card half">
								<a href="<?php the_field('block_2_link'); ?>">
									<?php foreach ($seasons as $season => $season_id): ?>
										<span class="image box-shadow <?php echo "$season-content" ?>" style="background-image: url(<?php the_field('block_2_image', $season_id)?>)"></span>
									<?php endforeach; ?>
									<h1 class="text-center font-color-white"><?php the_field('block_2_text'); ?></h1>
								</a>
							</div>
							<div class="cell small-6 shop-card">
								<a href="<?php the_field('block_3_link'); ?>">
									<?php foreach ($seasons as $season => $season_id): ?>
										<span class="image box-shadow <?php echo "$season-content" ?>" style="background-image: url(<?php the_field('block_3_image', $season_id)?>)"></span>
									<?php endforeach; ?>
									<h1 class="text-center font-color-white"><?php the_field('block_3_text') ?></h1>
								</a>
								</div>
								<div class="cell small-6 shop-card">
									<a href="<?php the_field('block_4_link'); ?>">
										<?php foreach ($seasons as $season => $season_id): ?>
											<span class="image box-shadow <?php echo "$season-content" ?>" style="background-image: url(<?php the_field('block_4_image', $season_id)?>)"></span>
										<?php endforeach; ?>
										<h1 class="text-center font-color-white"><?php the_field('block_4_text') ?></h1>
									</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Always Resourceful Block -->
					<div class="row expanded margin-bottom margin-top bg-cover relative" style="padding: 6rem 0; margin-bottom: 4rem;">
						<?php foreach ($seasons as $season => $season_id): ?>
							<span class="fake-bg <?php echo "$season-content" ?>" style="background-image: url(<?php the_field('always_resourceful_bg', $season_id); ?>)"> </span>
						<?php endforeach; ?>
						<div class="grid-container expanded margin-top" >
							<div class="grid-x grid-margin-x">
								<div class="cell small-12"><h1 class="text-center margin-top font-color-primary text-shadow">Always Resourceful</h1></div>
								<div class="cell small-12"><h4 class="text-center subheading font-color-white margin-top margin-bottom text-shadow">Tips to avoid injury and improve performance</h4></div>
								<div class="cell small-12 "><p class="text-center margin-top margin-bottom"><a href="/resources/runner-resources/"><button class="sites-button button-contract subheading">Read More</button></a></p></div>
							</div>
						</div>
					</div>
					<!-- Recent Blogs -->
					<?php $blog_query = new WP_Query( array(
						'posts_per_page' => 4,
						'category__not_in' => get_category_by_slug( 'podcast-episode' )->term_id,
					)); 
					?>
					<div class="grid-container row blog-card-loop">
						<div class="cell small-12 blog-card-loop"><h1 class="text-center margin-bottom margin-top">Recent Blog Posts</h1></div>
						<div class="grid-x grid-margin-x margin-top margin-bottom">
							<?php while ($blog_query->have_posts()): $blog_query->the_post();?>
								<div class="cell medium-6">
									<?php get_template_part('template-parts/content-card', 'post') ?>
								</div>
							<?php endwhile; wp_reset_postdata();?> 

							<?php $pcast_query = new WP_Query( array(
								'posts_per_page' 	=> 4,
								'post_type'			=> 'post',
								'category_name' => 'podcast-episode'
								)); ?>

								<!-- Recent Podcast Episodes -->
								<div class="cell small-12 blog-card-loop"><h1 class="text-center margin-bottom margin-top">Recent Podcast Episodes</h1></div>
								<?php while ($pcast_query->have_posts()): $pcast_query->the_post();?>
									<div class="cell medium-6">
										<?php get_template_part('template-parts/content-card-podcast'); ?>
									</div>
								<?php endwhile; wp_reset_postdata();?>
							</div>
						</div>


						<?php echo do_shortcode('[mailchimp_signup_row]'); ?>


						<!-- Social Sorting Section -->
						<div class="grid-container margin-top social-sorting">
							<div class="grid-x grid-margin-x grid-margin-y">
								<div class="cell small-12">
									<h1 class="text-center">Getting Social</h1>	
								</div>
								<div class="cell small-4">
									<p class="text-center social-icon"><a href="<?php the_field('facebook_url',get_option( 'page_on_front' )); ?>"><i class="social-icon-round fa fa-facebook-f"></i></a></p>
									<p class="text-center follow-button"><a href="<?php the_field('facebook_url',get_option( 'page_on_front' )); ?>"><button class="sites-button button-contract">FOLLOW</button></a></p>
									<div class='round-corners'><?php the_field('facebook_embed_code'); ?></div>
								</div>
								<div class="cell small-4">
									<p class="text-center"><a href="<?php the_field('twitter_url',get_option( 'page_on_front' )); ?>"><i class="social-icon-round fa fa-twitter"></i></a></p>
									<p class="text-center follow-button"><a href="<?php the_field('twitter_url',get_option( 'page_on_front' )); ?>"><button class="sites-button button-contract">FOLLOW</button></a></p>
									<div class='round-corners'><?php echo do_shortcode( get_field('twitter_embed_code') ); ?></div>
								</div>
								<div class="cell small-4">
									<p class="text-center"><a href="<?php the_field('instagram_url',get_option( 'page_on_front' )); ?>"><i class="social-icon-round fa fa-instagram"></i></a></p>
									<p class="text-center follow-button"><a href="<?php the_field('instagram_url',get_option( 'page_on_front' )); ?>"><button class="sites-button button-contract">FOLLOW</button></a></p>
									<div class='round-corners'><?php echo do_shortcode( get_field('instagram_embed_code') ); ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>




				<?php
				get_footer();
