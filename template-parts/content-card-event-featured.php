
	<div class="cell small-12 event-card featured">
		<div class="grid-x grid-margin-x">
			<div class="cell large-4 date-block gradient-primary-md-gray-vertical">
				<?php $date = strtotime(get_field('event_date'));?>
				<i class="fa fa-star font-color-white"></i>
				<p class="month text-center font-color-white heading-font"><?php echo date( 'M', $date ); ?></p>
				<p class="day-of-week text-center font-color-white"><?php echo date( 'l', $date ); ?></p>
				<p class="day-date text-center font-color-white"><?php echo date( 'd', $date ); ?></p>
			</div>
			<div class="cell large-8 race-info-wrapper font-color-white" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'fp_large');?>')">
				<div class="race-info">
					<h4 class="font-color-white"><?php the_title(); ?></h4>
					<p class=""><?php the_field('event_location'); ?></p>
					<p class="" style="font-weight: 700;"><?php the_field('event_time'); ?></p>
					<a class="font-color-white" href="<?php the_permalink(); ?>">VIEW EVENT DETAILS <i class="fa fa-arrow-right"></i></a>
				</div>
			</div>
		</div>
	</div>