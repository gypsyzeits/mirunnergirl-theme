
<div class="cell small-12 event-content featured">
	<div class="grid-x grid-margin-x">
		<div class="cell large-12 date-block bg-primary-color">
			<header>
				<h1 class="font-color-white" style="display: inline-block; margin:0"><?php the_title(); ?></h1>
				<?php $date = strtotime(get_field('event_date'));?>
				<h2 class="text-right font-color-white subheading heading-date" style="display: inline-block; margin:0"" style="display: inline-block;"><?php echo date( 'm/d/y', $date ); ?></h2>
			</header>
		</div>
		<?php if (get_the_post_thumbnail_url()): ?>
			<div class="cell small-12">
				<div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
					<div class="orbit-wrapper" style="    background: #eeeeee;">
						<div class="orbit-controls">
							<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
							<button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
						</div>
						<ul class="orbit-container">

							<li class="is-active orbit-slide">
								<figure class="orbit-figure">
									<div class="event-slider" style="background-image: url(<?php echo the_post_thumbnail_url('fp-medium'); ?>);">
								</figure>
							</li>

							<?php for ($i=1; $i <=4 ; $i++):?>
								<?php if(get_field("additional_image_$i")): ?>
									<li class="orbit-slide">
										<figure class="orbit-figure">
											<div class="event-slider" style="background-image: url(<?php echo get_field("additional_image_$i"); ?>);">
										</figure>
									</li>
								<?php endif; ?>
							<?php endfor; ?>
						</ul>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="cell small-12 race-info-wrapper margin-top">
			<div class="race-info">
				<h2 class="subheading font-color-primary"><?php the_field('event_location'); ?></h2>
				<h3 class="subheading"><?php the_field('event_time'); ?></h3>
				<?php if (get_field('event_link')): ?>
					<p>More information at <a href="<?php echo get_field('event_link') ?>"><?php echo get_field('event_link'); ?></a></p>
				<?php endif; ?>
				
				<?php $etags = wp_get_post_terms($post->ID, 'event_tags'); ?>
				<?php if (count($etags)): ?>
					<p>
						<?php foreach ($etags as $etag):?>
							<a href="/event/?event_tag=<?php echo $etag->slug; ?>"><?php echo $etag->name; ?></a>
						<?php endforeach; ?>
					</p>
				<?php endif; ?>
				
				<?php $location = get_field('event_location_(map)');
				if( !empty($location) ):
					?>
					<div class="acf-map">
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
					</div>
				<?php endif; ?>
				<?php the_content(); ?>
			</div>
		</div>

		<div class="cell small-12 race-info-wrapper margin-top">
			<?php 
			$query = new WP_Query( array(
				'post_type' => 'event',
				'orderby'		=> 'meta_value_num',
				'order'			=> 'ASC',
				'meta_query' => array(
					array(
						'key'			=> 'event_date',
						'value'		=> date('Y-m-d'),
						'compare'	=> '>=',
						'type'		=> 'DATE',
					))));
					?>
					<?php if ($query->have_posts()): ?>
						<h2 class="font-color-primary margin-bottom">Upcoming Events</h2>
						<div class="grid-x grid-margin-y">

							<?php while ($query->have_posts()) {
								$query->the_post();
								get_template_part('template-parts/content-card-event');
							} ?>
							<?php wp_reset_query(); ?>

						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

