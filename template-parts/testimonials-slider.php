<?php 
$test_query = new WP_Query (array(
	'post_type'		=> 'testimonial',
	));?>	
	<div class='grid-container testimonials'>
		<div class="orbit" role="region" aria-label="testimonials" data-orbit>
			<div class="orbit-wrapper padding-top">
				<div class="orbit-controls">
					<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
					<button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
				</div>
				<ul class="orbit-container">

					<?php while ($test_query->have_posts()): $test_query->the_post(); ?>

						<li class="orbit-slide">
							<figure class="orbit-figure">
								<div class='grid-x grid-margin-x'>
								<div class="cell small-6 small-offset-3 medium-offset-1 medium-2 portrait-wrapper">
									<img class="portrait box-shadow" src="<?php the_post_thumbnail_url( 'medium' ); ?>">
								</div>
								<div class="cell small-8 small-offset-2 medium-offset-0 medium-8">
									
									<?php the_content(); ?>
									<h4 class="font-color-white subheading name text-shadow">- <?php the_title(); ?></h4>
								</div>
								
							</div>
						</figure>
					</li>
				<?php endwhile; wp_reset_query() ?>
			</ul>
		</div>
	</div>
</div>