<div class="cell medium-6">
	<a href="<?php the_permalink() ?>">
		<span class="masthead"style="background-image: url(<?php the_post_thumbnail_url( $size = 'large' ) ?>)">
			<p class="font-color-secondary date text-center"><?php the_date( ); ?></p>
		</span>
	</a>
	<a href="<?php the_permalink(); ?>">
		<h3 class="subheading font-color-primary text-center"><?php the_title(); ?></h3>
	</a>
	<p><?php the_excerpt(); ?></p>
	<p class="text-center margin-top margin-bottom"><a href="<?php the_permalink(); ?>"><button class="sites-button button-contract subheading">Read More</button></a></p>
</div>