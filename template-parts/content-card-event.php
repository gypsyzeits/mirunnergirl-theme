<div class="cell small-12 event-card small <?php if (get_field('featured_event') == 1) echo 'featured-small'; ?>">
	<div class="grid-x grid-margin-x">
		<div class="cell small-4 medium-3 large-2 date-block ">
			<div class="gradient-primary-md-gray-vertical">
				<i class="fa fa-star font-color-white"></i>
				<?php $date = strtotime(get_field('event_date'));?>
				<p class="month text-center font-color-white heading-font"><?php echo date( 'M', $date ); ?></p>
				<p class="day-of-week text-center font-color-white"><?php echo date( 'l', $date ); ?></p>
				<p class="day-date text-center font-color-white"><?php echo date( 'd', $date ); ?></p>
			</div>
		</div>
		<div class="cell auto race-info-wrapper")">
			<div class="race-info">
				<h4 class="font-color-primary"><?php the_title(); ?></h4>
				<p class=""><?php the_field('event_location'); ?></p>
				<p class=""><?php the_field('event_time'); ?></p>
				<?php the_excerpt(); ?>
				<a class="" href="<?php the_permalink(); ?>">VIEW EVENT DETAILS <i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
	</div>
</div>