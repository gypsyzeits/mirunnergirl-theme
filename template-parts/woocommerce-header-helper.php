<?php get_template_part( 'template-parts/featuredimage-seasons-only'); ?>
<?php $seasons = array( 	
	'winter'	=> get_id_by_slug('home-winter'),
	'spring'	=> get_id_by_slug('home-winter/home-spring'),
	'summer'	=> get_id_by_slug('home-winter/home-summer'), 
	'fall'		=> get_id_by_slug('home-winter/home-fall'));  ?>
	<div class="grid-container-full-width">
		<div class="grid-x">
			<div class="cell small-12  bg-primary-color">
				
				<p class="text-center store-announcement-wrapper subheading font-color-white margin-top">
					<?php if( get_field('shop_announcement', wc_get_page_id('shop')) !== '' ): ?>
					
					
					<span class="season-icon-wrapper">
						<?php foreach ($seasons as $key => $value):?>
							<img class="season-icon <?php echo $key.'-content' ?>" src="<?php echo get_template_directory_uri().'/dist/assets/images/'.$key.'-icon.png' ?>">
						<?php endforeach; ?>
					</span>
					<span class=store-announcement><?php the_field('shop_announcement', wc_get_page_id('shop')); ?></span>
					<span class="season-icon-wrapper">
						<?php foreach ($seasons as $key => $value):?>
							<img class="season-icon <?php echo $key.'-content' ?>" src="<?php echo get_template_directory_uri().'/dist/assets/images/'.$key.'-icon.png' ?>">
						<?php endforeach; ?>
					</span>
					<?php endif; ?>
				</p>
				
			</div>
			<div class="cell small-12 margin-top show-for-medium shop-heading-menu">
				<?php wp_nav_menu( array('theme_location'=>'shop_heading_menu'));  ?>
			</div>
			<?php if (is_shop()): ?>
				<div class="cell small-12 gradient-primary-md-gray">
					<?php get_template_part( 'template-parts/testimonials-slider' ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>