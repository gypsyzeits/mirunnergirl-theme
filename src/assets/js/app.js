import $ from 'jquery';
import whatInput from 'what-input';
import './lib/jquery.cookie.js';
import './lib/season-switcher';
import './lib/acf-map.js';
import './lib/jquery.geocomplete.min.js';


window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

$(document).foundation();
