


$( document ).ready(function() {
	console.log( "start season picker" );
	$('.season-picker-wraper').show();

    //FIGURE OUT NUMBER OF DAYS INTO YEAR

    var now = new Date();
    var start = new Date(now.getFullYear(), 0, 0);
    var diff = (now - start) + ((start.getTimezoneOffset() - now.getTimezoneOffset()) * 60 * 1000);
    var oneDay = 1000 * 60 * 60 * 24;
    var day = Math.floor(diff / oneDay);
    console.log('Day of year: ' + day);

    var season = $.cookie('season');

    if (! season ){
		if ( day < 79 ) { //first day of spring 2018 
			season = 'winter';
		}
	    else if (  day < 172 ){ //first day of summer
	    	season = 'spring';
	    }
	    else if ( day < 265 ) { // first day of fall
	    	season = 'summer';
	    }
	    else if  ( day < 355 ) { // first day of winter
	    	season = 'fall';
	    }
	    else {
	    	season = 'winter';
	    }	
	}	

	var sheet = 'app.css'; // start with winter

	switch (season){
		case 'spring':
		 	sheet = 'app-spring.css';
		 	break;
		case 'summer':
		 	sheet = 'app-summer.css';
		 	break;
		case 'fall':
			sheet = 'app-fall.css';
			break;
	}
	



	$('#main-stylesheet-css').attr('href','/wp-content/themes/mirunnergirl/dist/assets/css/' + sheet);
	$("#"+season+"-video").attr('src', $("#"+season+"-video").data("yturl") );

	fixheight();


    $('.season-button').on('click', function (e) {
    	//e.stopPropagation();
    	var sheet = $(this).data('sheet');
    	var season = $(this).data('season');
    	$('.season-video').attr('src', '');
    	$("#"+season+"-video").attr('src', $("#"+season+"-video").data("yturl") );
    	$('#main-stylesheet-css').attr('href','/wp-content/themes/mirunnergirl/dist/assets/css/' + sheet);
    	$.cookie('season', season, { expires: 7 } );
    	console.log('switch to '+season+' '+sheet );
		fixheight();
		$('#off-canvas-menu').foundation('close');	
    } );


function fixheight(){
    	var sliderheight = -1;
    	$('.hero-wrapper .orbit-container').each(function(){
    		sliderheight = sliderheight > $(this).height() ? sliderheight : $(this).height();}
    		);
    	$('.hero-wrapper .orbit-container').height(sliderheight);
    }

});