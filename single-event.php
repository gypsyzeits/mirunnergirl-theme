<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.

 $seasons = array( 	
	'winter'	=> get_id_by_slug('home-winter'),
	'spring'	=> get_id_by_slug('home-winter/home-spring'),
	'summer'	=> get_id_by_slug('home-winter/home-summer'), 
	'fall'		=> get_id_by_slug('home-winter/home-fall')); 

	?>

	<?php foreach ($seasons as $key => $value): ?>
		<header class="featured-hero <?php echo $key.'-content' ?>" role="banner" data-interchange="[<?php echo get_the_post_thumbnail_url( $value, 'featured-small' ); ?>, small], [<?php echo get_the_post_thumbnail_url( $value, 'featured-medium' ); ?>, medium], [<?php echo get_the_post_thumbnail_url( $value, 'featured-large' ); ?>, large], [<?php echo get_the_post_thumbnail_url( $value, 'featured-xlarge' ); ?>, xlarge]">
		</header>
	<?php endforeach ?>
<div class="main-container">
	<div class="main-grid">
		<main class="main-content-full-width">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'event' ); ?>
			<?php endwhile; ?>
		</main>
		<?php //get_sidebar(); ?>
	</div>
</div>

<?php get_footer();
