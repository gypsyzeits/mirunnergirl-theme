<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.

$seasons = array( 	
	'winter'	=> get_id_by_slug('home-winter'),
	'spring'	=> get_id_by_slug('home-winter/home-spring'),
	'summer'	=> get_id_by_slug('home-winter/home-summer'), 
	'fall'		=> get_id_by_slug('home-winter/home-fall')); 

	?>

	<?php foreach ($seasons as $key => $value): ?>
		<header class="featured-hero <?php echo $key.'-content' ?>" role="banner" data-interchange="[<?php echo get_the_post_thumbnail_url( $value, 'featured-small' ); ?>, small], [<?php echo get_the_post_thumbnail_url( $value, 'featured-medium' ); ?>, medium], [<?php echo get_the_post_thumbnail_url( $value, 'featured-large' ); ?>, large], [<?php echo get_the_post_thumbnail_url( $value, 'featured-xlarge' ); ?>, xlarge]">
			<h1 class="page-hero-name text-center font-color-white text-shadow">Races & Events</h1>
		</header>
	<?php endforeach ?>


	<div class="main-container">
		<div class="main-grid">
			<main class="main-content-full-width">
				<div class='grid-container'>
					<div class="grid-x grid-margin-y grid-margin-x">
						<div class="cell small-12">
							<?php 
							$race_content = get_post(get_id_by_slug('race-and-event-content'));
							$output = apply_filters( 'the_content', $race_content->post_content );
							echo $output;
							?>
						</div>
						<div class="cell medium-6">
							<p class="text-center"><a href="/product/event-submission/"><button class="sites-button button-contract">Submit an Event</button></a></p>
						</div>
						<div class="cell medium-6">
							<p class="text-center"><a href="/runs-races/featured-mitten-state-races/"><button class="sites-button button-contract">Featured Events</button></a></p>
						</div>
						<div class="cell">
							<form class="event-date-search-form" action="/event" method="get">
								<div class="grid-container">
									<div class="grid-x grid-margin-x">
										<div class="cell medium-6 large-3">
											<label>EVENT TYPE</label>
											<select name="event_tag"">
												<option  class="event-search-input" value="" selected="all"></option>
												<?php $tags = get_terms('event_tags', array( 'hide_empty' => false, )); ?>
												<?php foreach ($tags as $tag):?>
													<option value="<?php echo $tag->slug; ?>" <?php if (isset($_GET['event_tag']) && $_GET['event_tag'] == $tag->slug) echo 'selected'; ?>><?php echo $tag->name; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="cell medium-6 large-3">
											<label>FROM
												<input class="event-search-input" type="date" name="startdate" value="<?php if( isset($_GET['startdate'])) { echo $_GET['startdate']; } else {echo date( 'Y-m-d' );} ?>">
											</label>
										</div>
										<div class="cell medium-6 large-3">
											<label>TO
												<input class="event-search-input" type="date" name="enddate" value="<?php if( isset($_GET['enddate'])){ echo $_GET['enddate']; }?>">
											</label>
										</div>
										<div class="cell medium-6 large-3 form-search-submit">
											<button class="sites-button align-bottom"><input type="submit" name="submit" value="Find Events"></button>
										</div>
										<script type="text/javascript">
											if ( $('[type="date"]').prop('type') != 'date' ) {
												$('[type="date"]').datepicker({dateFormat: "yy-mm-dd"});
											}
										</script>
									</div>
								</div>
							</form>

						</div>

						<script type="text/javascript">
							$( document ).ready(function() {
						// 	$(".form-search-submit").hide();
						// 	$(".event-search-input").change( $("form.event-date-search-form").submit() );
					});
				</script>

				<?php if ( have_posts() ) : ?>
					<?php /* Start the Loop */ ?>
					<?php  $featured_ids = array(); ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php if ( get_field('featured_event') && count($featured_ids) < 3):  ?>
							<?php get_template_part( 'template-parts/content-card-event-featured' ); ?>
							<?php $featured_ids[] = get_the_ID(); ?>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php rewind_posts(); ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php if ( ! in_array(get_the_ID(), $featured_ids) ): ?>
							<?php get_template_part( 'template-parts/content-card-event' ); ?>
							<?php $featured_ids[] = get_the_ID(); ?>
						<?php endif; ?>
					<?php endwhile; ?>

				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; // End have_posts() check. ?>

				<?php /* Display navigation to next/previous pages when applicable */ ?>
				<?php
				if ( function_exists( 'foundationpress_pagination' ) ) :
					?>
					<div class="cell small-12 text-center">
						<?php foundationpress_pagination(); ?>
					</div>
					<?php
				elseif ( is_paged() ) :
					?>


					<nav id="post-nav">
						<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
						<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
					</nav>

				<?php endif; ?>
			</div>
		</div>
	</main>
	<?php //get_sidebar(); ?>
</div>
</div>

<?php get_footer();
