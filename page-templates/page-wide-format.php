<?php
/*
Template Name: Wide Format
*/
get_header(); ?>
<?php get_template_part( 'template-parts/featured-image' ); ?>
<div class="tab-buttons text-center">
	<a class='tab-button bg-primary-color' href="/blog/"><img src="<?php echo get_template_directory_uri().'/dist/assets/images/blog-tab-button.png' ?>"></a>
	<a class='tab-button bg-color-gray' href="/shop/"><img src="<?php echo get_template_directory_uri().'/dist/assets/images/store-tab-button.png' ?>"></a>
	<a class='tab-button bg-primary-color' href="/category/podcast-episode/"><img src="<?php echo get_template_directory_uri().'/dist/assets/images/podcast-tab-button.png' ?>"></a>
</div>

<div class="main-container">
	<div class="main-grid">
		<div class="full-width-content">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
				<?php //comments_template(); ?>
			<?php endwhile; ?>
		</div>
	</div>
</div>

<?php echo do_shortcode('[shop_gear_row]'); ?>
<div class="grid-container" style="padding: 3rem 0;">
	<div class="grid-x grid-margin-y grid-margin-x">
		<div class="cell medium-6">
			<?php the_field('middle_text'); ?>
		</div>
		<div class="cell medium-6">
			<img class="box-shadow" src="<?php echo get_field('middle_image'); ?>">
		</div>
	</div>
</div>
<?php echo do_shortcode('[sponsors_row]'); ?>
<div class="grid-container" style="padding: 3rem 0;">
<div class="grid-x">
	<div class="cell small-12">
		<?php the_field('bottom_text'); ?>
	</div>
</div>
</div>
<?php echo do_shortcode('[mailchimp_signup_row]'); ?>
<?php get_footer();
