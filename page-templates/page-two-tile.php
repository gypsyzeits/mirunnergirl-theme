<?php
/*
Template Name: Two Tile
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>
<div class="main-container">
	<div class="main-grid">
		<main class="main-content-full-width">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
				<?php //comments_template(); ?>
			<?php endwhile; ?>
		</main>
	</div>
</div>
<?php echo do_shortcode('[sponsors_row]'); ?>
<div class="grid-container margin-top">
	<div class="grid-x grid-margin-y grid-margin-x">
		<?php for ($i=1; $i < 5 ; $i++): ?>
		<div class="cell medium-6">
			<span class="tile header-image box-shadow" style="background-image: url(<?php the_field("block_image_".$i); ?>)"></span>
			<h2 class="font-color-primary text-center"><?php the_field("block_header_".$i); ?></h2>
			<p class="text-center"><?php echo get_field("block_text_".$i); ?></p>
			<p class="text-center"><button class="sites-button button-contract text-center"><a href="<?php the_field("block_button_link_".$i); ?>"><?php the_field("block_button_text_".$i); ?></a></button></p>
		</div>
		<?php endfor; ?>
	</div>
</div>
<?php echo do_shortcode('[shop_gear_row]'); ?>

<div class="margin-top margin-bototm">
	<h1 class="text-center">Latest Blog Posts</h1>
<?php echo do_shortcode('[recent_blog_row]'); ?>
</div>
<?php echo do_shortcode('[mailchimp_signup_row]'); ?>
<?php get_footer();
