<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes();?> >
<head>
	<meta charset="<?php bloginfo('charset');?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700|Pacifico" rel="stylesheet">
	<?php wp_head();?>
</head>
<body <?php body_class();?>>

	<?php if (get_theme_mod('wpt_mobile_menu_layout') === 'offcanvas'): ?>
		<?php get_template_part('template-parts/mobile-off-canvas');?>
	<?php endif;?>
	<div class="grid-container">
		<div class="grid-x season-picker-wraper grid-padding-x hide-for-small-only grid-margin-y" style="display:none">
			<div class="cell  medium-5 large-5 large-offset-2"><h3 class="font-color-primary text-right" >Choose your season!</h3></div>
			<div class="cell medium-7 large-5" id="season-picker">
				<img id="spring-button" class="season-button" data-sheet="app-spring.css" data-season="spring" src="<?php echo get_template_directory_uri() . '/dist/assets/images/spring-button.png' ?>">
				<img id="summer-button" class="season-button" data-sheet="app-summer.css" data-season="summer" src="<?php echo get_template_directory_uri() . '/dist/assets/images/summer-button.png' ?>">
				<img id="fall-button" class="season-button"  data-sheet="app-fall.css" data-season="fall" src="<?php echo get_template_directory_uri() . '/dist/assets/images/fall-button.png' ?>">
				<img id="winter-button" class="season-button" data-sheet="app.css" data-season="winter" src="<?php echo get_template_directory_uri() . '/dist/assets/images/winter-button.png' ?>">
				
				<a href="#" data-toggle="search-bar-top" >
					<i class="fa fa-search"></i>
				</a>
				<a href="/cart/">
					<i class="fa fa-shopping-cart"></i>(<?php echo WC()->cart->get_cart_contents_count(); ?>)
				</a>
			</div>
			
			<div class="cell large-6 large-offset-6 hide" data-toggler=".show" id="search-bar-top"><?php get_search_form($echo = true);?></div>
		</div>
	</div>
	<div class="sticky-container" data-sticky-container >
		<header class="site-header sticky" role="banner" data-sticky data-options="marginTop:0;" style="" data-sticky-on="small" >
			
			<div class="grid-container" >
				<div class="site-title-bar"> <?php foundationpress_title_bar_responsive_toggle();?>
					<div class="title-bar-left">

						<span class="site-mobile-title title-bar-title">
							<a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><img src="<?php echo get_template_directory_uri() . '/dist/assets/images/mi-runner-girl-logo.jpg' ?>"></a>
						</span>
					</div>
					<div class='title-bar-right'><button aria-label="<?php _e('Main Menu', 'foundationpress');?>" class="menu-icon" type="button" data-toggle="<?php foundationpress_mobile_menu_id();?>"></button></div>
				</div>

				<nav class="site-navigation top-bar" role="navigation">
					<div class="top-bar-left">
						<div class="site-desktop-title top-bar-title">
							<a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><img src="<?php echo get_template_directory_uri() . '/dist/assets/images/mi-runner-girl-logo.jpg' ?>"></a>
						</div>
					</div>
					<div class="top-bar-right">
						<?php foundationpress_top_bar_r();?>

						<?php if (!get_theme_mod('wpt_mobile_menu_layout') || get_theme_mod('wpt_mobile_menu_layout') === 'topbar'): ?>
							<?php get_template_part('template-parts/mobile-top-bar');?>
						<?php endif;?>
					</div>
				</nav>

			</div>

		</header>
	</div>
	<div id="scroll-to-top-target"></div>