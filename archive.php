<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>
<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.

 $seasons = array( 	
	'winter'	=> get_id_by_slug('home-winter'),
	'spring'	=> get_id_by_slug('home-winter/home-spring'),
	'summer'	=> get_id_by_slug('home-winter/home-summer'), 
	'fall'		=> get_id_by_slug('home-winter/home-fall')); 

	?>

	<?php foreach ($seasons as $key => $value): ?>
		<header class="featured-hero <?php echo $key.'-content' ?>" role="banner" data-interchange="[<?php echo get_the_post_thumbnail_url( $value, 'featured-small' ); ?>, small], [<?php echo get_the_post_thumbnail_url( $value, 'featured-medium' ); ?>, medium], [<?php echo get_the_post_thumbnail_url( $value, 'featured-large' ); ?>, large], [<?php echo get_the_post_thumbnail_url( $value, 'featured-xlarge' ); ?>, xlarge]">
			<h1 class="page-hero-name text-center font-color-white text-shadow"><?php echo single_cat_title( $prefix = '', $display = true ); ?></h1>
		</header>
	<?php endforeach ?>

<div class="main-container-full-width">
	<div class="main-grid">
		<main class="main-content-full-width">
			<div class='grid-container blog-card-loop'>
				<div class="grid-x grid-margin-y grid-margin-x">
					<div class="cell small-12 text-center">
						<?php echo category_description(  ); ?>
					</div>
					<?php if ( have_posts() ) : ?>

						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="cell medium-4">
							<?php get_template_part( 'template-parts/content-card-post' ); ?>
							</div>
						<?php endwhile; ?>

					<?php else : ?>
						<?php get_template_part( 'template-parts/content', 'none' ); ?>

					<?php endif; // End have_posts() check. ?>

					<?php /* Display navigation to next/previous pages when applicable */ ?>
					<?php
					if ( function_exists( 'foundationpress_pagination' ) ) :
						?><div class="cell small-12 text-center">
							<?php foundationpress_pagination(); ?>	
						</div>
					<?php
					elseif ( is_paged() ) :
						?>
						<nav id="post-nav">
							<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
							<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
						</nav>
					<?php endif; ?>
				</div>
			</div>
		</main>
	</div>
</div>

<?php get_footer();
