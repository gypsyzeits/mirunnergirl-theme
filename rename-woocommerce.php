
<?php
/**
 * Basic WooCommerce support
 * For an alternative integration method see WC docs
 * http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>
<header class="featured-hero" role="banner" data-interchange="[<?php echo get_the_post_thumbnail_url(wc_get_page_id('shop'), 'featured-small' ); ?>, small], [<?php echo get_the_post_thumbnail_url(wc_get_page_id('shop'), 'featured-medium'  ); ?>, medium], [<?php echo get_the_post_thumbnail_url(wc_get_page_id('shop'), 'featured-large' ); ?>, large], [<?php echo get_the_post_thumbnail_url(wc_get_page_id('shop'), 'featured-xlarge'  ); ?>, xlarge]">
	<h1 class="page-hero-name text-center font-color-white text-shadow">SHOP</h1>
</header>
<?php $seasons = array( 	
	'winter'	=> get_id_by_slug('home-winter'),
	'spring'	=> get_id_by_slug('home-winter/home-spring'),
	'summer'	=> get_id_by_slug('home-winter/home-summer'), 
	'fall'		=> get_id_by_slug('home-winter/home-fall'));  ?>
	<div class="grid-container-full-width">
		<div class="grid-x">
			<div class="cell small-12  bg-primary-color">
				<p class="text-center store-announcement-wrapper subheading font-color-white margin-top">
					<span class="season-icon-wrapper">
						<?php foreach ($seasons as $key => $value):?>
							<img class="season-icon <?php echo $key.'-content' ?>" src="<?php echo get_template_directory_uri().'/dist/assets/images/'.$key.'-icon.png' ?>">
						<?php endforeach; ?>
					</span>
					<span class=store-announcement><?php the_field('shop_announcement', wc_get_page_id('shop')); ?></span>
					<span class="season-icon-wrapper">
						<?php foreach ($seasons as $key => $value):?>
							<img class="season-icon <?php echo $key.'-content' ?>" src="<?php echo get_template_directory_uri().'/dist/assets/images/'.$key.'-icon.png' ?>">
						<?php endforeach; ?>
					</span>
				</p>
			</div>
			<div class="cell small-12 margin-top show-for-medium bg-">
				<?php wp_nav_menu( array('menu'=>'shop_heading_menu'));  ?>
			</div>
			<?php if (is_shop()): ?>
				<div class="cell small-12 gradient-primary-md-gray">
					<?php get_template_part( 'template-parts/testimonials-slider' ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="main-container-full-width">
		<div class="main-grid">
			<main class="main-content-full-width">
				<div class="grid-container">
					<div class="grid-x">
						<?php woocommerce_content(); ?>
					</div>
				</div>
			</main>
		</div>
	</div>

	<?php 
	global $post;
	if ($post->post_name === 'calender-event'):
	?>
	<script type="text/javascript">
		$('document').ready( function(){
			$('.quantity').hide();
			$('.woocommerce-Price-amount').hide();
			$('#gform_totals_2 li:first-child').hide();
			$('#gform_totals_2 li:nth-child(2)').hide();
			$('#field_2_7').after('<li><div id="map-picker"></div></li>');
		});

	</script>
<script type="text/javascript">
//map.js
 
//Set up some of our variables.
var map; //Will contain map object.
var marker = false; ////Has the user plotted their location marker? 
        
//Function called to initialize / create the map.
//This is called when the page has loaded.
function initMap() {
 
    //The center location of our map.
    var centerOfMap = new google.maps.LatLng(43.728640793776215, -84.61763502837704);
 
    //Map options.
    var options = {
      center: centerOfMap, //Set center.
      zoom:6 //The zoom value.
    };
 
    //Create the map object.
    map = new google.maps.Map(document.getElementById('map-picker'), options);
 
    //Listen for any clicks on the map.
    google.maps.event.addListener(map, 'click', function(event) {                
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if(marker === false){
            //Create the marker.
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map,
                draggable: true //make it draggable
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event){
                markerLocation();
            });
        } else{
            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
        }
        //Get the marker's location.
        markerLocation();
    });
}
        
//This function will get the marker's current location and then add the lat/long
//values to our textfields so that we can save the location.
function markerLocation(){
    //Get location.
    var currentLocation = marker.getPosition();
    //Add lat and lng values to a field that we can save.
    document.getElementById('input_2_8').value = currentLocation.lat(); //latitude
    document.getElementById('input_2_16').value = currentLocation.lng(); //longitude
}
        
        
//Load the map when the page has finished loading.
google.maps.event.addDomListener(window, 'load', initMap);
</script>	


<?php endif; ?>
	<?php get_footer();
