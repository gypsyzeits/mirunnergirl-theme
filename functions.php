<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Format comments */
require_once( 'library/class-foundationpress-comments.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/class-foundationpress-top-bar-walker.php' );
require_once( 'library/class-foundationpress-mobile-walker.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** Configure ACF fields */
//require_once( 'library/acf-fields-config.php');

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/class-foundationpress-protocol-relative-theme-assets.php' );

// Usage:
// get_id_by_slug('any-page-slug');

function get_id_by_slug($page_slug) {
	$page = get_page_by_path($page_slug);
	if ($page) {
		return $page->ID;
	} else {
		return null;
	}
}


function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyCtLNn4cl81nm9KPjKa7Ckqv4Hfc2WwRoY';
	
	return $api;
	
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function enqueue_google_maps_js(){
	wp_enqueue_script( 'google_maps', $src = 'https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCtLNn4cl81nm9KPjKa7Ckqv4Hfc2WwRoY',$in_footer = true);
}

add_action('wp_enqueue_scripts', 'enqueue_google_maps_js');

/**
 * Create a taxonomy
 *
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 *
 * @param string  Name of taxonomy object
 * @param array|string  Name of the object type for the taxonomy object.
 * @param array|string  Taxonomy arguments
 * @return null|WP_Error WP_Error if errors, otherwise null.
 */
function register_event_tags() {

	$labels = array(
		'name'                  => _x( 'Event Tags', 'Taxonomy plural name', 'text-domain' ),
		'singular_name'         => _x( 'Event Tag', 'Taxonomy singular name', 'text-domain' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'hierarchical'      => false,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => true,
		'query_var'         => true,
		'capabilities'      => array(),
	);

	register_taxonomy( 'event_tags', array( 'event' ), $args );
}

add_action( 'init', 'register_event_tags' );

//Include Events Parameters for Events Archive Pages
function events_modify_main_query($query){

	if ( ! $query->is_main_query()){ return;} 
	if ( ! is_post_type_archive( $post_types = 'event' ) ){ return;} 
	if ( is_admin() ){return;}
	
	$query->query_vars['posts_per_page'] = 10;
	$query->query_vars['orderby'] = 'meta_value';
	$query->query_vars['meta_key'] = 'event_date';
	$query->query_vars['order'] = 'ASC';

	if ( isset($_GET['event_tag']) && $_GET['event_tag']){
		$query->query_vars['tax_query'] = array(
			array(
				'terms'		=> 	$_GET['event_tag'],
				'taxonomy'=>	'event_tags',
				'field'		=>	'slug',
			)
		);
	}


	$meta_query = array();
	//$meta_query = $query->get('meta_query');
	
	//if no date is set use today's date
	if ( isset($_GET['startdate'])){
		$meta_query[] = array(
			'key'			=> 	'event_date',
			'value'		=>	$_GET['startdate'],
			'compare'	=>	'>=',
			'type'		=> 	'DATE',
		);
	}
	else{
		$meta_query[] = array(
			'key'			=> 	'event_date',
			'value'		=>	date('Y-m-d'),
			'compare'	=>	'>=',
			'type'		=> 	'DATE',
		);
	}		

	//end date
	if ( isset($_GET['enddate']) && $_GET['enddate'] ){
		$meta_query[] = array(
			'key'			=> 	'event_date',
			'value'		=>	$_GET['enddate'],
			'compare'	=>	'<=',
			'type'		=> 	'DATE',
		);
	}

	$query->set('meta_query', $meta_query);

	
}

add_action('pre_get_posts', 'events_modify_main_query');

function excerpt_lengths($length){
	global $post;
	if ($post->post_type == 'event'){
		return 12;
	}

	return 20;
}

add_action( 'excerpt_length', 'excerpt_lengths');

//woocommerce support
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


// Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
function jk_dequeue_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
	unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
	unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}

 //$gmaps_url = add_query_arg( 'language', str_replace( '_', '-', get_locale() ), 'https://maps.google.com/maps/api/js?key=AIzaSyCct0nZWaekc-3uzeTCLuwTUNBPePdi3xI' );
//wp_enqueue_script( 'google-maps', $gmaps_url, array(), false, true );

/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */ 
function woo_related_products_limit() {
	global $product;
	
	$args['posts_per_page'] = 3;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}

/**
 * Removes coupon form, order notes, and several billing fields if the checkout doesn't require payment
 * Tutorial: http://skyver.ge/c
 */
function sv_free_checkout_fields() {
	
	// Bail we're not at checkout, or if we're at checkout but payment is needed
	if ( function_exists( 'is_checkout' ) && ( ! is_checkout() || ( is_checkout() && WC()->cart->needs_payment() ) ) ) {
		return;
	}
	
	// remove coupon forms since why would you want a coupon for a free cart??
	remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
	
	// Remove the "Additional Info" order notes
	add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );
	// Unset the fields we don't want in a free checkout
	function unset_unwanted_checkout_fields( $fields ) {

		// add or remove billing fields you do not want
		// list of fields: http://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/#section-2
		$billing_keys = array(
			'billing_company',
			'billing_phone',
			'billing_address_1',
			'billing_address_2',
			'billing_city',
			'billing_postcode',
			'billing_country',
			'billing_state',
		);
		// unset each of those unwanted fields
		foreach( $billing_keys as $key ) {
			unset( $fields['billing'][$key] );
		}
		
		return $fields;
	}
	add_filter( 'woocommerce_billing_fields', 'unset_unwanted_checkout_fields' );
	
	// A tiny CSS tweak for the account fields; this is optional
	function print_custom_css() {
		echo '<style>.create-account { margin-top: 6em; }</style>';
	}
	add_action( 'wp_head', 'print_custom_css' );
}
add_action( 'wp', 'sv_free_checkout_fields' );





function streamline_free_checkout($fields){
	if ( function_exists( 'is_checkout' ) && ( ! is_checkout() || ( is_checkout() && WC()->cart->needs_payment() ) ) ) {
		return $fields;
	}
	//var_dump($fields);
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_address_1']);
	unset($fields['billing']['billing_address_2']);
	unset($fields['billing']['billing_city']);
	unset($fields['billing']['billing_postcode']);
	unset($fields['billing']['billing_country']);
	unset($fields['billing']['billing_state']);
	unset($fields['billing']['billing_phone']);

	return $fields;
}


add_filter( 'woocommerce_checkout_fields', 'streamline_free_checkout');

//Special Widgets


//Upcoming Races Widget

class upcoming_races extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'classname' => 'upcoming_races',
			'description' => 'A widget that displays upcoming races',
		);
		parent::__construct( 'upcoming_races', 'Upcoming Races', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		$events = new WP_Query( array(
			'post_type' => 'event',
			'orderby'		=> 'meta_value',
			'meta_key'	=> 'event_date',
			'order'			=> 'ASC',
			'posts_per_page'	=> 3,
			'meta_query'	=> array( array(
				'key'			=> 	'event_date',
				'value'		=>	date('Y-m-d'),
				'compare'	=>	'>=',
				'type'		=> 	'DATE',

			)),
		));
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		//var_dump($events);
		if ($events->have_posts()){
			while($events->have_posts()):
				$events->the_post();
				?>
				<div class="margin-top">
					<p><STRONG class="font-color-primary subheading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></STRONG></p>
					<p><?php the_field('event_location'); echo ' - '.date('m/d', strtotime(get_field('event_date')));?></p>
				</div>
				<?php
			endwhile;
			wp_reset_postdata();
			?>
			<p class="text-center margin-top"><a href="/event/" class="subheading font-color-primary" style="font-size: .8rem;">Learn More</a></p>
			<?php

		}
		else{
			?>
			<p>Sorry there are new upcoming races.</p>
			<?php
		}

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}
}

add_action( 'widgets_init', function(){
	register_widget( 'upcoming_races' );
});



class sponsors_widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'classname' => 'sponsors_widget',
			'description' => 'A widget that displays sponsors',
		);
		parent::__construct( 'sponsors_widget', 'Sponsors Widget', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		$sponsors = new WP_Query( array(
			'post_type' => 'sponsor',
			'orderby'		=> 'rand',
			'posts_per_page'	=> 10,
		));

		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		if ($sponsors->have_posts()){
			?>
			<div class="orbit" role="region" aria-label="logo-carousel" data-orbit>
				<div class="orbit-wrapper sponsors">
					<div class="grid-x">
						<div class="orbit-controls">
							<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
							<button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
						</div>
					</div>
					<ul class="orbit-container">
						<?php
						while($sponsors->have_posts()):
							$sponsors->the_post();
							?>
							<li class="orbit-slide">

								<a target="_blank" href="<?php the_field("sponsor_url");?>"><img class="orbit-image"  src="<?php the_field("sponsor_image");?>" alt="Space"></a>
							</li>
							<?php
						endwhile;
						wp_reset_postdata();
						?>
					</ul>
				</div>
			</div>
			<?php
		}

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}
}

add_action( 'widgets_init', function(){
	register_widget( 'sponsors_widget' );
});



class featured_product_widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'classname' => 'featured_product_widget',
			'description' => 'A widget that displays featured products',
		);
		parent::__construct( 'featured_product_widget', 'Orbit Featured Product Widget', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		$products = new WP_Query( array(
			'post_type' => 'product',
			'orderby'		=> 'rand',
			'posts_per_page'	=> 8,
			'tax_query' => array(
				array(
					'taxonomy' => 'product_visibility',
					'field'    => 'name',
					'terms'    => 'featured',
				),
			),
		));

		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		if ($products->have_posts()){
			?>
			<div class="orbit" role="region" aria-label="logo-carousel" data-orbit>
				<div class="orbit-wrapper sponsors">
					<div class="grid-x">
						<div class="orbit-controls">
							<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
							<button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
						</div>
					</div>
					<ul class="orbit-container">
						<?php
						while($products->have_posts()):
							$products->the_post();
							?>
							<li class="orbit-slide">

								<a target="_blank" href="<?php the_permalink();?>"><img class="orbit-image"  src="<?php echo get_the_post_thumbnail_url( );?>"></a>
							</li>
							<?php
						endwhile;
						wp_reset_postdata();
						?>
					</ul>
				</div>
			</div>
			<?php
		}

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}
}

add_action( 'widgets_init', function(){
	register_widget( 'featured_product_widget' );
});

function wc_add_event_to_calender( $order_id ){
	require_once(ABSPATH . 'wp-admin/includes/media.php');
	require_once(ABSPATH . 'wp-admin/includes/file.php');
	require_once(ABSPATH . 'wp-admin/includes/image.php');
	$order = new WC_Order($order_id);
	$order_items = $order->get_items();
	$order_is_only_cal_events = true;
	$tagSlugs = array();
	
	foreach ($order_items as $item) {
		$lat = 10;
		$long = 10;
		$location = '';
		if($item->get_name() === 'Event Submission'){
			$metas = $item->get_meta_data();
			$args = array( 'post_status' => 'publish', 'post_type' => 'event');
			$additional_image_count = 0;
			$additional_image_field_id = array(
				'field_5ab8f08183760',
				'field_5ab8f08c83761',
				'field_5ab8f08e83762',
				'field_5ab8f0bd83763',
			);

			
			foreach ($metas as $meta) {
				$metum = $meta->get_data();
				//var_dump($meta);

				switch ($metum['key']) {
					case 'Title':
					$args['post_title'] = $metum['value']; 
					break;
					
					case 'Description':
					$args['post_content'] = strip_tags($metum['value'], '<a><p>');
					break;

					case 'Event Type':
						//var_dump($metum['value']);
					$slugs = explode(', ',$metum['value'] );

					$tagSlugs = $slugs;
					break;
					$order->add_order_note($metum['value']);

					default:
					break;
				}
				
				

			}


			$event_id = wp_insert_post( $args, $wp_error = false );

			wp_set_post_terms($event_id, $tagSlugs, 'event_tags');

			
			$order->add_order_note("added event: ".$args['post_title']);
			// var_dump($metas);
			// die();

			foreach ($metas as $meta) {
				
				echo 'hello';
				$metum = $meta->get_data();
				
				switch ($metum['key']) {

					case 'Featured/Regular Event':
					if( $metum['value'] === 'Featured Event $150.00 ($150.00)'){
						update_field('field_5ab8e9c16038b', '1', $event_id);
					}
					break;

					case 'Date':
					update_field('field_5ab8e90660387',date('Y-m-d', strtotime($metum['value'])), $event_id);
					break;

					case 'Time':
					update_field( 'field_5ab8e94160389',$metum['value'], $event_id);
					break;

					case 'Location':
					update_field('field_5ab8e92b60388',$metum['value'], $event_id);
					$location = $metum['value'];
					break;
					
					case 'Website':
					update_field('field_5ab8ea026038c',$metum['value'], $event_id);
					break;
					
					case 'Featured Image':
					$result = set_post_thumbnail( $event_id, media_sideload_image( strip_tags($metum['value']), 0, $desc = null, $return = 'id' ));
						//var_dump($result);
					break;

					case 'Location Map Latitude':
					echo "hello";
					$lat = $metum['value'];
					var_dump($lat);

					break;

					case 'Location Map Longitude':
					$long = $metum['value'];
					break;
					
					case 'Additional Image':

					update_field( $additional_image_field_id[ $additional_image_count ], media_sideload_image( strip_tags($metum['value']), 0, $desc = null, $return = 'id' ), $event_id);
					$additional_image_count++;
					break;

					default:
					break;

				}
			}

			if ( $lat && $long ){
				$map = array(
					'address' => $location,
					'lat'			=> $long,
					'lng'		=> $lat,
					'zoom'	=> '1'
				);
				update_field('field_5ab8e9a36038a', $map, $event_id);
			}

		}
		else{
			$order_is_only_cal_events = false;
		}

	}
	if($order_is_only_cal_events){
		$order->update_status('completed');
		$order->add_order_note('order contained only events. Marked Complete');
	}
	update_post_meta( $order_id, '_wc_events_added', 'yes' );
}

add_action('woocommerce_order_status_processing','wc_add_event_to_calender');



function sv_wc_add_order_meta_box_action( $actions ) {
	global $theorder;

    // bail if the order has been paid for or this action has been run
    // if ( get_post_meta( $theorder->get_id(), '_wc_events_added', true ) ) {
    //     return $actions;
    // }

    // add "mark printed" custom action
	$actions['wc_add_events_action'] = __( 'Add Events To Calender', 'my-textdomain' );
	return $actions;
}
add_action( 'woocommerce_order_actions', 'sv_wc_add_order_meta_box_action' );

function wc_add_events_action_by_order_id(){
	global $post;

	wc_add_event_to_calender($post->ID);		

}

add_action( 'woocommerce_order_action_wc_add_events_action', 'wc_add_events_action_by_order_id' );

//import_events();

//add custom styles to visual editor


// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );



// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own setting 
		array(  
			'title' => 'button',  
			'block' => 'button',  
			'classes' => 'sites-button',
			'wrapper' => true,
		),
		array(  
			'title' => 'button contracted',  
			'block' => 'button',  
			'classes' => 'sites-button button-contract',
			'wrapper' => true,
		),
		array(  
			'title' => 'H1PrimaryColor',  
			'block' => 'h1',  
			'classes' => 'font-color-primary',
		),
		array(  
			'title' => 'H2PrimaryColor',  
			'block' => 'h2',  
			'classes' => 'font-color-primary',
		),
		array(  
			'title' => 'H3PrimaryColor',  
			'block' => 'h3',  
			'classes' => 'font-color-primary',
		),
		array(  
			'title' => 'H4PrimaryColor',  
			'block' => 'h4',  
			'classes' => 'font-color-primary',
		),
		array(  
			'title' => 'H5PrimaryColor',  
			'block' => 'h5',  
			'classes' => 'font-color-primary',
		),
		array(  
			'title' => 'H5PrimaryColor',  
			'block' => 'h5',  
			'classes' => 'font-color-primary',
		),
		array(  
			'title' => 'H6PrimaryColor',  
			'block' => 'h6',  
			'classes' => 'font-color-primary',
		),
		array(  
			'title' => 'H1PrimaryColorSubheading',  
			'block' => 'h1',  
			'classes' => 'font-color-primary subheading',
		),
		array(  
			'title' => 'H2PrimaryColorSubheading',  
			'block' => 'h2',  
			'classes' => 'font-color-primary subheading',
		),
		array(  
			'title' => 'H3PrimaryColorSubheading',  
			'block' => 'h3',  
			'classes' => 'font-color-primary subheading',
		),
		array(  
			'title' => 'H4PrimaryColorSubheading',  
			'block' => 'h4',  
			'classes' => 'font-color-primary subheading',
		),
		array(  
			'title' => 'H5PrimaryColorSubheading',  
			'block' => 'h5',  
			'classes' => 'font-color-primary subheading',
		),
		array(  
			'title' => 'H5PrimaryColorSubheading',  
			'block' => 'h5',  
			'classes' => 'font-color-primary subheading',
		),
		array(  
			'title' => 'H6PrimaryColorSubheading',  
			'block' => 'h6',  
			'classes' => 'font-color-primary subheading',
		),
		array(  
			'title' => 'H1Subheading',  
			'block' => 'h1',  
			'classes' => 'subheading',
		),
		array(  
			'title' => 'H2Subheading',  
			'block' => 'h2',  
			'classes' => 'subheading',
		),
		array(  
			'title' => 'H3Subheading',  
			'block' => 'h3',  
			'classes' => 'subheading',
		),
		array(  
			'title' => 'H4Subheading',  
			'block' => 'h4',  
			'classes' => 'subheading',
		),
		array(  
			'title' => 'H5Subheading',  
			'block' => 'h5',  
			'classes' => 'subheading',
		),
		array(  
			'title' => 'H5Subheading',  
			'block' => 'h5',  
			'classes' => 'subheading',
		),
		array(  
			'title' => 'H6Subheading',  
			'block' => 'h6',  
			'classes' => 'subheading',
		),


	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  

} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  

function mailchimp_signup_shortcode(){
	ob_start();
	?><!-- signup form -->
	<div class='row expanded gradient-primary-md-gray margin-top  relative email-legs-wrapper'>
		<div class="grid-container margin-top relative" style="z-index: 2;" >

			<div class="grid-x">

				<div class="cell medium-9 medium-offset-1 margin-top margin-bottom"><h1 class="font-color-white text-center text-shadow">Get the Best Tips & Updates!</h1></div>
				<div class="cell medium-9 medium-offset-1 margin-top margin-bottom"><h4 class="font-color-white text-shadow subheading text-center">Subscribe to my newsletter. I'll keep you updated with fitness tips, recipes, and more!</h4></div>
				<div class="cell medium-9 medium-offset-1 relative" style="z-index: 3;"><?php echo do_shortcode( get_field('mailchimp_shortcode', get_option( 'page_on_front' )), $ignore_html = false ); ?></div>


			</div>
		</div>
		<span class="fake-bg email-legs" style="background-image: url(<?php echo get_template_directory_uri() . '/dist/assets/images/email-legs.png';?>)"></span>
	</div>
	<?php return ob_get_clean();
}


function sponsors_row_shortcode(){
	ob_start();?>
	<!-- Friends & Sponsors -->
	<div class='row expanded gradient-primary-md-gray margin-top margin-bottom'>
		<div class="grid-container expanded margin-top" >
			<div class="grid-x grid-margin-x" data-equalizer data-equalize-on="medium" id="test-eq">
				<div class="cell small-12"><h1 class="text-center margin-top font-color-white">Friends & Supporters</h1></div>
				<div class="cell small-12"><h4 class="text-center subheading font-color-white margin-top margin-bottom">Learn More About Becoming a Partner With Michigan Runner Girl</h4></div>
				<div class="cell small-12 "><p class="text-center margin-top margin-bottom"><a href="/partners/"><button class="sites-button button-contract subheading">Learn More</button></a></p></div>
			</div>


			<?php
			$logo_query = new WP_Query(array(
				'post_type' => 'sponsor',
				'orderby'   => 'rand',
				'nopaging'  => true,
			));
			?>
			<div class="orbit" role="region" aria-label="logo-carousel" data-orbit>
				<div class="orbit-wrapper sponsors">
					<div class="grid-x">
						<div class="orbit-controls">
							<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
							<button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
						</div>
					</div>
					<ul class="orbit-container">
						<?php
						$logo_counter = 0;
						while ($logo_query->have_posts()): $logo_query->the_post();?>
						<?php if ($logo_counter == 0): ?>
							<li class="orbit-slide">
								<div class="grid-x grid-margin-x grid-margin-y">
								<?php endif;?>
								<div class="cell medium-3 small-6">
									<a target="_blank" href="<?php the_field("sponsor_url");?>"><img class="orbit-image"  src="<?php the_field("sponsor_image");?>" alt="Space"></a>
								</div>
								<?php $logo_counter++;if ($logo_counter == 4): ?>
							</div>
						</li>
						<?php $logo_counter = 0; endif;?>
					<?php endwhile; wp_reset_postdata();?>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php
return ob_get_clean();
}

function recent_blog_posts_row(){
	ob_start();?>
	<?php $blog_query = new WP_Query( array(
		'posts_per_page' => 4,
	)); 
	?>
	<div class="grid-container row blog-card-loop">
		<div class="grid-x grid-margin-x margin-top margin-bottom">
			<?php while ($blog_query->have_posts()): $blog_query->the_post();?>
				<div class="cell medium-6">
					<?php get_template_part('template-parts/content-card', 'post') ?>
				</div>
			<?php endwhile; wp_reset_postdata();?>
		</div>
	</div>
	<?php
	return ob_get_clean();
}


function shop_gear_row(){
	ob_start();?>
	<div class="shop-promo-row row gradient-primary-md-gray">
		<div class="grid-x grid-margin-x margin-top margin-bottom">
			<div class="cell medium-4 medium-offset-4 relative">
				<h1 class="font-color-white text-center margin-bottom margin-top" style="padding-top:3rem;">Shop Gear</h1>
				<p class='text-center'>
					<a href="/shop/"><button class="sites-button button-contract margin-top margin-bottom">SHOP NOW</button></a></p>
				</button>
				<span class="shop-row-hoody" style="background-image: url(<?php echo get_template_directory_uri().'/dist/assets/images/shop-hoody.png' ?>"></span>
			</div>
			<div class="cell small-12 show-for-small-only"><img style="margin: 0 10%; width: 80%" src="<?php echo get_template_directory_uri().'/dist/assets/images/shop-hoody.png' ?>"></div>
		</div>
	</div>
	<?php 
	return ob_get_clean();
}

add_shortcode('shop_gear_row', 'shop_gear_row');
add_shortcode('recent_blog_row', 'recent_blog_posts_row');
add_shortcode('sponsors_row', 'sponsors_row_shortcode');
add_shortcode('mailchimp_signup_row', 'mailchimp_signup_shortcode');


add_filter( 'script_loader_src', 'gfgp_api_key', 10, 2 );
function gfgp_api_key( $src, $handle ) {
	if( 'google-places' === $handle ) {
		$src = add_query_arg( array(
			'key' => 'AIzaSyCtLNn4cl81nm9KPjKa7Ckqv4Hfc2WwRoY'
		), $src );
	}
	return $src;
}

